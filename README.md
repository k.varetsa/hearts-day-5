# Hearts Programming Marathon

## Description

Welcome to the Hearts Programming Marathon repository! 
This project is part of a programming marathon participated by Kostiantyn Varetsa. 
In this marathon, participants work on creating the classic card game Hearts using Python.

This is truly one of the greatest card games ever devised for four players, each playing individually.

### The Pack
The standard 52-card pack is used.

### Object of the Game
To be the player with the lowest score at the end of the game. 
When one player hits the agreed-upon score or higher, the game ends; and the player with the lowest score wins.

### Card Values/scoring
At the end of each hand, players count the number of hearts they have taken as well as the queen of spades, 
if applicable. Hearts count as one point each and the queen counts 13 points.
Each heart - 1 point
The Q - 13 points
The aggregate total of all scores for each hand must be a multiple of 26.
The game is usually played to 100 points (some play to 50).
When a player takes all 13 hearts and the queen of spades in one hand, instead of losing 26 points, 
that player scores zero and each of his opponents score an additional 26 points.

### The Deal
Deal the cards one at a time, face down, clockwise. Each player gets 13 cards.

### The Play
The player holding the 2 of clubs after the pass makes the opening lead.

Each player must follow suit if possible. If a player is void of the suit led, a card of any other suit 
may be discarded. However, if a player has no clubs when the first trick is led, a heart or the queen of 
spades cannot be discarded. The highest card of the suit led wins a trick and the winner of that trick leads next. 
There is no trump suit.

The winner of the trick collects it and places it face down. Hearts may not be led until a heart or the queen of 
spades has been discarded. The queen does not have to be discarded at the first opportunity.

The queen can be led at any time.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)
- [Credits](#credits)

## Installation

To get started with the Hearts project, follow these steps:

1. Clone this repository to your local machine.
2. Navigate to the project directory.
3. Install the necessary dependencies by running `pip install -r requirements.txt`.

## Usage

Detailed instructions on how to run and interact with the Hearts game will be provided in the project documentation. 
Participants are encouraged to explore the codebase, contribute new features, and enhance the overall gaming experience.

### Day_1_homework

* A function which accepts a string and returns a dictionary where keys are all string's 
symbols and values are its according emersion probability in the string.

Usage example: 

> counting_letters("hello")
> 
>{'e': 20.0, 'h': 20.0, 'l': 40.0, 'o': 20.0}
> 
>counting_letters("Amigo")
> 
>{'m': 20.0, 'g': 20.0, 'i': 20.0, 'A': 20.0, 'o': 20.0}
> 
>counting_letters("I want to break free")
> 
>{'t': 10.0, 'I': 5.0, 'r': 10.0, 'e': 15.0, 'b': 5.0, 'a': 10.0, 'k': 5.0, 'f': 5.0, 'w': 5.0, 'n': 5.0, ' ': 20.0, 'o': 5.0}

* Implementing logic with help of ternary operator:

(Int variables x and у, return string)

if x < y - return x + y,

if x == y - return 0,

if x > y - return x - y,

if x == 0 та y == 0 return "game over"

Usage example:

>tern_op(2, 4)
> 
> '6'
> 
> tern_op(8, 4)
> 
> '4'
> 
>tern_op(4, 4)
>
> '0'
> 
> tern_op(0, 0)
> 
> 'game over'

## Contributing

If you're interested in contributing to the Hearts game project, follow these steps:

1. Fork the repository.
2. Create a new branch for your feature or bug fix.
3. Make your changes and commit them.
4. Push your changes to your fork.
5. Submit a pull request to the main repository.

## Credits

This repository owned by Kostiantyn Varetsa. 
Special thanks to CyberBionic Systematics (https://edu.cbsystematics.com/ua) and Konstantin Zivenko for their 
valuable input and efforts in making this project a success.
