from datetime import date
from typing import Optional


class Genre:
    def __init__(self, name: str, description: Optional[str] = None) -> None:
        self.name = name
        self.description = description

    def __repr__(self) -> str:
        return f"Genre ({self.name}, {self.description})"

    def __str__(self) -> str:
        return self.name


class Author:
    def __init__(
        self, first_name: str, last_name: str, birth_year: Optional[int] = None
    ) -> None:
        self.first_name = first_name
        self.last_name = last_name
        self.birth_year = birth_year

    def __repr__(self) -> str:
        return f"Author ({self.first_name}, {self.last_name}, {self.birth_year})"

    def __str__(self) -> str:
        return f"{self.first_name} {self.last_name}"

    def __eq__(self, other: "Author") -> bool:
        if not isinstance(other, Author):
            raise TypeError(
                f"for type Author and type {type(other)} operation is not implemented."
            )
        return (
            self.first_name == other.first_name
            and self.last_name == other.last_name
            and self.birth_year == other.birth_year
        )

    def __hash__(self) -> int:
        return hash((self.first_name, self.last_name, self.birth_year))


class Book:
    def __init__(
        self,
        name: str,
        language: str,
        year: int,
        *authors,
        isbn: str = None,
        genres: list[Genre] = None,
        description: Optional[str] = None,
    ) -> None:
        self.name = name
        self.description = description
        self.language = language
        self.year = year
        self.isbn = isbn
        self.authors = authors
        self.genres = genres

    def __repr__(self) -> str:
        return (
            f"Book ({self.name}, {self.description}, {self.language}, {self.year}, {self.isbn}, {self.authors}, "
            f"{self.genres})"
        )

    def __str__(self) -> str:
        author_names = ", ".join(str(x) for x in self.authors)
        existing_genres = ", ".join(str(x) for x in self.genres)
        return (
            f"Authors: {author_names}\n"
            f"Genres: {existing_genres}\n"
            f"Book: {self.name}\n"
            f"Language: {self.language}\n"
            f"Year: {self.year}\n"
            f"ISBN: {self.isbn}\n"
        )

    def __eq__(self, other: "Book") -> bool:
        if not isinstance(other, Book):
            raise TypeError(
                f"for type Book and type {type(other)} operation is not implemented."
            )
        return set(self.authors) == set(other.authors) and self.name == other.name

    def age(self) -> str:
        """Prints out the difference in years between yaer of book's publishing and current year."""
        current_year = date.today().year
        return f"This book was published {current_year - self.year} year(s) ago."


stoker = Author("Bram", "Stoker", 1847)
doe = Author("John", "Doe", 1868)
novel = Genre(
    "Novel",
    "A novel is a long, fictional narrative. "
    "The novel in the modern era usually makes use of a literary prose style. "
    "The development of the prose novel at this time was encouraged by innovations "
    "in printing, and the introduction of cheap paper in the 15th century.",
)

b1 = Book(
    "Dracula",
    "English",
    1992,
    stoker,
    doe,
    genres=[novel],
)
print(b1.age())
