def tern_op(a, b):
    """
    Ternary operator with a specific logic.
    :param a: int,
    :param b: int
    :return: str
    """
    return str(
        a + b if a < b else a - b if a > b else "game over" if a == 0 and b == 0 else 0
    )


if __name__ == "__main__":
    cases = ((1, 2, "3"), (2, 1, "1"), (0, 0, "game over"), (1, 1, "0"))
    for x, y, result in cases:
        func_res = tern_op(x, y)
        assert (
            func_res == result
        ), f"ERROR: tern_op({x}, {y}) returned {func_res}, but expected: {result}"
    try:
        tern_op(33, "df")
    except TypeError as te:
        print(f"Incorrect input type: operator {te}. Input must be only integers.")
